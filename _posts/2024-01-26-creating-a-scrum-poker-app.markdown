---
layout: post
title: "Building a Real-Time Scrum Poker Website with Node & Socket.IO"
date: 2025-01-25
categories: [Node.js, WebSockets, Render]
---

Hey guys! Today, I'd like to share with you the process I went through to build a real-time Scrum Poker website for my team's refinement meetings. We’d tried multiple online tools before, but they had too many ads, imposed fees, or stored data in ways we didn't like. So, I built my own lightweight solution using Node.js, Express, Socket.IO, and Render—all without using a database!

## The Idea

The concept behind this Scrum Poker tool was to provide a simple and free alternative for teams to estimate tasks during refinement sessions. My main goals were:

1. Real-time voting with Socket.IO  
2. No database to reduce costs and complexity  
3. A shareable link so my team could easily join a “room” with a simple code  
4. Automatic cleanup once the last user leaves (no persistent storage)  

By building a simple Node/Express server in a container, I could host it free of charge on Render. This gave me real WebSocket support—perfect for quickly updating all participants.

## Why No Database?

I considered using a database (like MongoDB or PostgreSQL) to store rooms and users. However, I realized it wasn't necessary:

• Cost & Complexity – Databases can lead to monthly fees or extra config. My app’s ephemeral data (only relevant during a single session) didn’t need persistence.  
• Privacy – Some teams are sensitive about storing user info. By avoiding a DB, I store nothing beyond what's in memory. The data vanishes once everyone leaves.  
• Simplicity – Fewer dependencies and less code to maintain.

## Hosting Choices

I initially considered Vercel or Netlify, both of which I've used in the past with great success, but real-time WebSockets can be tricky with serverless functions. Instead, I used Render:

• Free Tier – Perfect for a small, part-time service like Scrum Poker.  
• Persistent WebSockets – Render’s free web service runs continuously, so Socket.IO remains connected.  
• GitHub Integration – I simply push my code to GitHub, and Render auto-deploys on every commit.

## Building the Tool

Here's how I structured the code.

### 1. Server Setup (Node/Express + Socket.IO)

In a file named server.js:

Use the Express and Socket.IO libraries. Create an HTTP server, attach Socket.IO, and serve the static files from a “public” folder. Maintain an in-memory object named rooms, which holds all room data:

rooms = {  
  "roomCode": {  
    "users": {  
      "socketId": {  
        "name": "string",  
        "estimate": (number or null)  
      }  
    },  
    "reveal": boolean  
  }  
};

Whenever a socket connects, handle the “joinRoom”, “estimate”, “reveal”, “reset”, and “disconnect” events. The ephemeral data disappears once everyone leaves.

### 2. Front-End (HTML, CSS, and Socket Logic)

Inside a “public” folder, create:

• index.html – Landing page for entering room code and user name  
• room.html – Displays the voting interface and user list  
• client.js – Socket.IO client logic  
• styles.css – Your custom styling

The core logic in client.js listens to a “roomUpdated” event to re-render all users in the UI. Until “reveal” is true, each user sees only “?” for others’ estimates. Once “reveal” is triggered, the actual estimates appear.

### 3. Additional Features

I included a few nice-to-have functionalities on the front end:

• Highlighting the minimum and maximum votes on reveal  
• Displaying the average estimate among selected cards  
• Confetti if everyone votes the same (where min equals max)  

None of these require changes to the server, since all user data is broadcast via “roomUpdated”. The front end can derive min, max, and average from the user objects it receives.

## Wrapping Up

That's it! By storing everything in memory and leveraging free hosting on Render, I built a completely free, real-time Scrum Poker solution that my team can use without ads or fees. We love how lightweight it is—no sign-ups, no data retention, and no monthly expenses. [Check it out here!](https://ben-made-scrum-poker.onrender.com)

I hope this post inspires you to create your own real-time tools or at least encourages you to explore Socket.IO and free hosting platforms. Let me know if you have any questions or ideas for further improvements. Happy coding!
