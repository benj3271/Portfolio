---
layout: post
title:  "Building the Not Hotdog App Inspired by Silicon Valley"
date:   2023-03-18 00:05:38 -400
---

What if I told you there was an app on the market... As a huge fan of the TV show _Silicon Valley_, I recently decided to build a web app inspired by the hilarious "Not Hotdog" app featured in the series. The app uses a machine learning model to determine whether an uploaded image contains a hotdog or not. In this post, I'll walk you through the process of building this app using React and a machine learning API.

## Step 1: Setting up the React project

To begin, I created a new React project using [Create React App](https://reactjs.org/docs/create-a-new-react-app.html):

```
npx create-react-app not-hotdog-app
cd not-hotdog-app
```

## Step 2: Building the app's UI

With the project set up, I started working on the app's user interface. The main components included an image upload button, a display area for the uploaded image, and a banner to show the result (Hotdog or Not Hotdog). Overall it was nothing too complicated here. You can see the full code linked in a git repository at the end of this post.

## Step 3: Integrating the machine learning API

This was the tricky part. I had to find a way to figure out if the image was actually a hotdog or not. After some research, I integrated the Tensorflow machine learning API to classify the uploaded images as hotdogs or not hotdogs. Here's an example of how to call the API:

```
import * as tf from "@tensorflow/tfjs";
import * as mobilenet from "@tensorflow-models/mobilenet";

const model = await mobilenet.load();
        const imgTensor = tf.browser.fromPixels(image).resizeNearestNeighbor([224, 224]).toFloat().expandDims();
        const predictions = await model.classify(imgTensor);
        const topPrediction = predictions[0];

        if (topPrediction.className.toLowerCase().includes("hotdog")) {
            ...
```

## Step 4: Adding celebration animations

To make the app more satisfying, I added confetti and fireworks animations whenever the app identified a hotdog in the uploaded image:

```
 useEffect(() => {
    if (celebrating && fireworkCanvas.current) {
      const fireworks = setInterval(() => {
        confetti({
          particleCount: 100,
          startVelocity: 30,
          spread: 360,
          origin: {
            x: Math.random(),
            y: Math.random() - 0.2,
          },
          zIndex: 100,
          disableForReducedMotion: true,
        });
      }, 500);

      setTimeout(() => {
        clearInterval(fireworks);
      }, 5000);
    }
  }, [celebrating]);
  ```

## Step 5: Deploying the app

Finally, I deployed the app using [Netlify](https://www.netlify.com/), a popular platform for hosting static websites and web apps. After setting up the project on Netlify, I used the Netlify CLI to deploy the app:

```
npm install -g netlify-cli
npm run build
netlify login
netlify deploy --prod --dir build
```

## Wrapping Up

After deploying the app, I received a domain: [not-hotdog.netlify.app](https://not-hotdog.netlify.app). 

And there you have it! A fun, Silicon Valley-inspired Not Hotdog app built using React and the Tensorflow machine learning API. If you're a fan of the show, I hope you enjoyed this little tribute to the series. Happy coding!


## Check out the code

Check out the code on my GitHub at: [https://github.com/benj327/Not-Hotdog](https://github.com/benj327/Not-Hotdog). Enjoy!




