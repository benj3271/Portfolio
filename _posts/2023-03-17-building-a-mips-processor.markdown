---
layout: post
title: "Building a MIPS Single-Cycle Processor: A Two-Month Journey"
date: 2023-03-17 12:43:00 -0400
categories: MIPS Processor VHDL FPGA
---

## Introduction

Over the past two months, I have been working on a challenging assignment for my Computer Organization and Assembly Level Programming course. The goal of the project was to build a MIPS single-cycle processor using mainly structural VHDL. In this blog post, I will walk you through the process I followed with my team and the different stages I went through to complete this assignment.

## MIPS Single-Cycle Processor Design

The first step in building the processor was to create a high-level schematic of the interconnection between components. We started by drawing the components already instantiated in the provided skeleton processor code, then sketched in the remaining high-level components needed for the basic instructions. As the project progressed, I updated the schematic to accommodate any additional components and signals required by the set of instructions not included in the provided schematic.

## Component Implementation

Once the schematic was complete, we proceeded to implement various components of the processor, including:

1. **Control Logic**: I created a spreadsheet detailing the list of MIPS instructions to be supported in the project alongside their binary opcodes and funct fields. We then implemented the control logic module using VHDL and tested it using a testbench.
2. **Fetch Logic**: The instruction fetch logic updates the Program Counter (PC). We identified the control flow possibilities required for the instructions, drew a schematic for the instruction fetch logic, implemented it using VHDL, and tested it using Modelsim.
3. **Complete MIPS ALU**: I updated the ALU to meet the full requirements of the MIPS Instruction Set Architecture (ISA). This included designing and implementing a barrel shifter for shift instructions, updating the ALU to support additional operations, and integrating the new ALU into the MIPS datapath.

## Testing the Processor

Testing the processor required more effort than previous projects due to the interaction between instructions affecting every single component. We used the automated testing framework provided for the course, which allowed us to assemble, simulate, and compare our design with the MARS simulator. I created various assembly programs to test the processor and improve its performance.

## Synthesis

Finally, I synthesized our design to the DE2 board's FPGA to determine the maximum cycle time. We reported the maximum frequency the processor could run at which was 45 mhz and identified the critical path. We also discussed which components we could focus on to improve the frequency and overall performance.

## Conclusion

Building the MIPS single-cycle processor was a challenging and rewarding experience that taught me a lot about computer organization, VHDL, and processor design. The assignment pushed me to think critically about the design and implementation of each component and forced me to consider the interaction between instructions and how they affect the overall performance of the processor. This project required significant time and was a valuable learning experience. 


## Check out the code

Our project repository was fairly messy at the end of the semester, but you can check out the code and see the high level schematic at https://github.com/benj327/MIPS-Processor. Enjoy!
