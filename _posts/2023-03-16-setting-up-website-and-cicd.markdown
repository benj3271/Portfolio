---
layout: post
title: "Setting Up My Personal Website and CI/CD with Jekyll, Docker, and GitLab"
date: 2023-03-16 07:02:11 -500
categories: jekyll update
---

Hey everyone! Today I'm going to share my experience in setting up my personal website to showcase my resume, and using continuous integration and continuous deployment (CI/CD) to show off my DevOps skills. I used Jekyll, Docker, and GitLab for this project. Let's dive into the process, design choices, challenges I faced, and some key takeaways. Overall this took about 3 hours, so if you're thinking about something similar, plan accordingly.

## Choosing Jekyll for My Personal Website

I wanted a static site generator that was easy to use, had great community documentation, and offered various themes to kickstart my site. Jekyll, a Ruby-based static site generator, fit the bill perfectly. With its simple setup and the availability of numerous themes, it made designing my website a breeze. Plus, it integrates well with GitHub Pages and GitLab Pages for easy deployment.

## Integrating Docker and GitLab

I decided to use Docker to containerize my Jekyll site for easy setup and consistent builds across different environments. GitLab was my choice for version control and CI/CD because it's a powerful platform that's easy to configure with Jekyll and Docker, with built in Jekyll images to make the process faster. GitLab CI/CD enables automatic builds and deployments each time I push changes to my repository, keeping my website up-to-date effortlessly.

## Challenges and Learning Takeaways

One of the initial challenges I faced was setting up the Jekyll site. I learned that it's essential to have a well-structured folder hierarchy and to use the `_config.yml` file to configure various aspects of the site, such as the base URL and theme.

Another challenge was integrating Docker and GitLab for CI/CD. I had to create a `.gitlab-ci.yml` file to define the CI/CD pipeline, which involved specifying the Jekyll Docker image and defining build and deploy stages. After some trial and error, I managed to get everything working smoothly.

One issue I encountered after deploying the site was that the UI on GitLab Pages didn't match the local version. I realized that the asset paths needed to be adjusted by adding the `baseurl` to the `_config.yml` file and using the `{{ site.baseurl }}` variable in my HTML, CSS, and JavaScript files. Oops.

## Wrapping Up

Setting up my personal website with Jekyll, Docker, and GitLab was a great learning experience. I had the chance to work with various technologies, integrate CI/CD, and overcome challenges along the way. This project has not only helped me showcase my resume and DevOps skills, but also taught me valuable lessons in web development and deployment.

I hope this post has been helpful and maybe even inspired you to create your own personal website. It's not too hard. Happy coding!

## Check out the code

Check out the full code for everything at https://gitlab.com/benj3271/Portfolio
