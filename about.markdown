---
layout: page
title: "About"
permalink: /about/
---

# Ben Johnson

- Email: [johnsonben327@gmail.com](mailto:johnsonben327@gmail.com)
- LinkedIn: [/in/ben-johnson-swe-1998](https://www.linkedin.com/in/ben-johnson-swe1998/)
- GitHub: [github.com/benj327](https://github.com/benj327)

**Active Department of Defense Top Secret Security Clearance**

## Education

**Iowa State University**   (Graduating December 2023)
_Bachelor of Science in Cybersecurity Engineering_  
Ames, IA

- Relevant Coursework: Data Structures, Analysis of Algorithms, Software Development Techniques, Network Protocols and Security, Computer Architecture, Embedded Systems, Application of Cryptography, Cyber Security Tools and Concepts, Operating Systems, and Digital Forensics
- Activities: President of Computer Science/Software Engineering Club, Member of Information Assurance Student Group, Cyber Defense Competition (3rd Place/14 teams in 2022), ICPC Team (3rd Place/29 teams at Regionals in 2023), ISU CTF Team, ATHENA Lab Research Assistant, and Alliance Jiu Jitsu Team Member
- Senior Design: Created a Smart Locker for students to check out supplies using a Raspberry Pi, Flask API, MySQL database, and a JS/HTML/CSS front end to interact with pre-existing hardware and a touch screen display

## Technical Skills

**Languages**: Java, Python, C, SQL, JavaScript/HTML/CSS, and Bash  
**Frameworks**: Spring, Flask, React, Selenium  
**Developer Tools**: Git, Docker, Kubernetes, Ansible, GitLab, AWS, Linux, MySQL, Firebase, JUnit, and Mockito  
**Security Tools**: Splunk, pfSense, Nmap, Wireshark, Nessus, DoD ACAS, Metasploit, and Autopsy

## Experience

**Software Engineering Intern** (January 2022 - December 2022)  
_The MITRE Corporation_  
USSTRATCOM/Offutt AFB, NE

- Assigned to the Nuclear Command, Control, and Communication modernization project
- Independently implemented a security automation solution using ansible to ensure STIG compliance, bringing time per machine from 40 hours to 5 minutes, saving the Air Force approximately $300,000 in 2022 alone
- Conducted SAST testing on applications using GitLab, and integrated these tests into existing CI/CD pipeline
- Created environments to test various security solutions with Linux machines in VirtualBox, in Docker containers, and using AWS instances. Presented findings to senior leadership at the Air Force Lifecycle Management Center
- Collaborated with a startup to integrate new DevSecOps tools into existing platforms using Python and Docker

**Software Engineering Intern** (May 2019 - December 2020)  
_Nationwide Insurance_  
Des Moines, IA

- Led an increase in unit testing coverage from 12%-90% using Mockito and then implemented an automated testing suite using Selenium for a project which has since processed over $100 million in commercial insurance policies
- Participated in a team hackathon and advanced to the semi-finals by developing a policy web form tool; saving agents an est. 5min. per form leveraging Python and Google Maps API; the tool is currently used company-wide
- Developed RESTful API using Spring and altered the Angular front-end to integrate it into the application

